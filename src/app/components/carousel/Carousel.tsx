"use client";

import React from "react";
import CarouselItem, { CarouselItemProps } from "./CarouselItem";
import Slider from "react-slick";

function Carousel() {
  const carouselItems: CarouselItemProps[] = [
    {
      title: "17 Years of excellent in",
      subTitle: "Construction industry",
      linkAction: "/",
      bgImage:
        "https://images.unsplash.com/photo-1541888946425-d81bb19240f5?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80",
    },
    {
      title: "17 Years of excellent in",
      subTitle: "Construction industry",
      linkAction: "/",
      bgImage:
        "https://images.unsplash.com/photo-1541888946425-d81bb19240f5?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80",
    },
  ];
  return (
    <Slider
      className="banner-carousel banner-carousel-1 mb-0"
      autoplay={true}
      speed={600}
      dots={true}
      arrows={true}
      slidesToShow={1}
      slidesToScroll={1}
      prevArrow={
        <button
          type="button"
          className="carousel-control left"
          aria-label="carousel-control"
        >
          <i className="fas fa-chevron-left"></i>
        </button>
      }
      nextArrow={
        <button
          type="button"
          className="carousel-control right"
          aria-label="carousel-control"
        >
          <i className="fas fa-chevron-right"></i>
        </button>
      }
    >
      {carouselItems.map((item) => (
        <CarouselItem
          key={item.title}
          title={item.title}
          subTitle={item.subTitle}
          bgImage={item.bgImage}
          linkAction={item.linkAction}
        />
      ))}
    </Slider>
  );
}

export default Carousel;
