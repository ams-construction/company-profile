import React from "react";
import ContactUsBar from "./ContactUsBar";
import Navbar from "./Navbar";

function Header() {
  return (
    <>
      <header id="header" className="header-one">
        <ContactUsBar />

        <Navbar />
      </header>
    </>
  );
}

export default Header;
