"use client";
import React, { useEffect, useState } from "react";

export interface FactProps {
  count: number;
  title: string;
  logo?: string;
}

function Facts(props: FactProps) {
  const [dataCount, setDataCount] = useState(0);

  return (
    <>
      <div className="ts-facts-img">
        <img
          loading="lazy"
          src="https://images.placeholders.dev"
          alt="facts-img"
        />
      </div>
      <div className="ts-facts-content">
        <h2 className="ts-facts-num">
          <span className="counterUp" data-count="1789">
            {props.count}
          </span>
        </h2>
        <h3 className="ts-facts-title">{props.title}</h3>
      </div>
    </>
  );
}

export default Facts;
