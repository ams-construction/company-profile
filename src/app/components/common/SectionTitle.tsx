import React from "react";

export interface SectionTitleProps {
  title: string;
  subTitle: string;
}

function SectionTitle({ title, subTitle }: SectionTitleProps) {
  return (
    <div className="row text-center">
      <div className="col-lg-12">
        <h2 className="section-title">{title}</h2>
        <h3 className="section-sub-title">{subTitle}</h3>
      </div>
    </div>
  );
}

export default SectionTitle;
