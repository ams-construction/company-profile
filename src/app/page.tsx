import Carousel from "./components/carousel/Carousel";
import Facts from "./components/common/Facts";
import Footer from "./components/common/Footer";
import Header from "./components/common/Header";
import SectionTitle from "./components/common/SectionTitle";
import TopBar from "./components/common/TopBar";

export default function Home() {
  return (
    <div className="body-inner">
      <TopBar />

      <Header />

      <Carousel />

      <section className="call-to-action-box no-padding">
        <div className="container">
          <div className="action-style-box">
            <div className="row align-items-center">
              <div className="col-md-8 text-center text-md-left">
                <div className="call-to-action-text">
                  <h3 className="action-title">
                    We understand your needs on construction
                  </h3>
                </div>
              </div>
              <div className="col-md-4 text-center text-md-right mt-3 mt-md-0">
                <div className="call-to-action-btn">
                  <a className="btn btn-dark" href="#">
                    Request Quote
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="ts-features" className="ts-features">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="ts-intro">
                <h2 className="into-title">About Us</h2>
                <h3 className="into-sub-title">We deliver landmark projects</h3>
                <p>
                  We are rethoric question ran over her cheek When she reached
                  the first hills of the Italic Mountains, she had a last view
                  back on the skyline of her hometown Bookmarksgrove, the
                  headline of Alphabet Village and the subline of her own road,
                  the Line Lane.
                </p>
              </div>

              <div className="gap-20"></div>

              <div className="row">
                <div className="col-md-6">
                  <div className="ts-service-box">
                    <span className="ts-service-icon">
                      <i className="fas fa-trophy"></i>
                    </span>
                    <div className="ts-service-box-content">
                      <h3 className="service-box-title">
                        We've Repution for Excellence
                      </h3>
                    </div>
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="ts-service-box">
                    <span className="ts-service-icon">
                      <i className="fas fa-sliders-h"></i>
                    </span>
                    <div className="ts-service-box-content">
                      <h3 className="service-box-title">
                        We Build Partnerships
                      </h3>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-md-6">
                  <div className="ts-service-box">
                    <span className="ts-service-icon">
                      <i className="fas fa-thumbs-up"></i>
                    </span>
                    <div className="ts-service-box-content">
                      <h3 className="service-box-title">
                        Guided by Commitment
                      </h3>
                    </div>
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="ts-service-box">
                    <span className="ts-service-icon">
                      <i className="fas fa-users"></i>
                    </span>
                    <div className="ts-service-box-content">
                      <h3 className="service-box-title">
                        A Team of Professionals
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-lg-6 mt-4 mt-lg-0">
              <h3 className="into-sub-title">Our Values</h3>
              <p>
                Minim Austin 3 wolf moon scenester aesthetic, umami odio
                pariatur bitters. Pop-up occaecat taxidermy street art, tattooed
                beard literally.
              </p>

              <div
                className="accordion accordion-group"
                id="our-values-accordion"
              >
                <div className="card">
                  <div
                    className="card-header p-0 bg-transparent"
                    id="headingOne"
                  >
                    <h2 className="mb-0">
                      <button
                        className="btn btn-block text-left"
                        type="button"
                        data-toggle="collapse"
                        data-target="#collapseOne"
                        aria-expanded="true"
                        aria-controls="collapseOne"
                      >
                        Safety
                      </button>
                    </h2>
                  </div>

                  <div
                    id="collapseOne"
                    className="collapse show"
                    aria-labelledby="headingOne"
                    data-parent="#our-values-accordion"
                  >
                    <div className="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life
                      accusamus terry richardson ad squid. 3 wolf moon officia
                      aute, non cupidata
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div
                    className="card-header p-0 bg-transparent"
                    id="headingTwo"
                  >
                    <h2 className="mb-0">
                      <button
                        className="btn btn-block text-left collapsed"
                        type="button"
                        data-toggle="collapse"
                        data-target="#collapseTwo"
                        aria-expanded="false"
                        aria-controls="collapseTwo"
                      >
                        Customer Service
                      </button>
                    </h2>
                  </div>
                  <div
                    id="collapseTwo"
                    className="collapse"
                    aria-labelledby="headingTwo"
                    data-parent="#our-values-accordion"
                  >
                    <div className="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life
                      accusamus terry richardson ad squid. 3 wolf moon officia
                      aute, non cupidata
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div
                    className="card-header p-0 bg-transparent"
                    id="headingThree"
                  >
                    <h2 className="mb-0">
                      <button
                        className="btn btn-block text-left collapsed"
                        type="button"
                        data-toggle="collapse"
                        data-target="#collapseThree"
                        aria-expanded="false"
                        aria-controls="collapseThree"
                      >
                        Integrity
                      </button>
                    </h2>
                  </div>
                  <div
                    id="collapseThree"
                    className="collapse"
                    aria-labelledby="headingThree"
                    data-parent="#our-values-accordion"
                  >
                    <div className="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life
                      accusamus terry richardson ad squid. 3 wolf moon officia
                      aute, non cupidata
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="facts" className="facts-area dark-bg">
        <div className="container">
          <div className="facts-wrapper">
            <div className="row">
              <div className="col-md-3 col-sm-6 ts-facts mt-5 mt-sm-0">
                <Facts title="Total Project" count={99} />
              </div>

              <div className="col-md-3 col-sm-6 ts-facts mt-5 mt-sm-0">
                <Facts title="Staff Members" count={44} />
              </div>

              <div className="col-md-3 col-sm-6 ts-facts mt-5 mt-md-0">
                <Facts title="Hours Of Work" count={40} />
              </div>

              <div className="col-md-3 col-sm-6 ts-facts mt-5 mt-md-0">
                <Facts title="Region Experience" count={10} />
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="ts-service-area" className="ts-service-area pb-0">
        <div className="container">
          <div className="row text-center">
            <div className="col-12">
              <h2 className="section-title">We Are Specialists In</h2>
              <h3 className="section-sub-title">What We Do</h3>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-4">
              <div className="ts-service-box d-flex">
                <div className="ts-service-box-img">
                  <img
                    loading="lazy"
                    src="https://images.placeholders.dev"
                    alt="service-icon"
                  />
                </div>
                <div className="ts-service-box-info">
                  <h3 className="service-box-title">
                    <a href="#">Home Construction</a>
                  </h3>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipiscing elit
                    Integer adipiscing erat
                  </p>
                </div>
              </div>

              <div className="ts-service-box d-flex">
                <div className="ts-service-box-img">
                  <img
                    loading="lazy"
                    src="https://images.placeholders.dev"
                    alt="service-icon"
                  />
                </div>
                <div className="ts-service-box-info">
                  <h3 className="service-box-title">
                    <a href="#">Building Remodels</a>
                  </h3>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipiscing elit
                    Integer adipiscing erat
                  </p>
                </div>
              </div>

              <div className="ts-service-box d-flex">
                <div className="ts-service-box-img">
                  <img
                    loading="lazy"
                    src="https://images.placeholders.dev"
                    alt="service-icon"
                  />
                </div>
                <div className="ts-service-box-info">
                  <h3 className="service-box-title">
                    <a href="#">Interior Design</a>
                  </h3>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipiscing elit
                    Integer adipiscing erat
                  </p>
                </div>
              </div>
            </div>

            <div className="col-lg-4 text-center">
              <img
                loading="lazy"
                className="img-fluid"
                src="https://images.placeholders.dev"
                alt="service-avater-image"
              />
            </div>

            <div className="col-lg-4 mt-5 mt-lg-0 mb-4 mb-lg-0">
              <div className="ts-service-box d-flex">
                <div className="ts-service-box-img">
                  <img
                    loading="lazy"
                    src="https://images.placeholders.dev"
                    alt="service-icon"
                  />
                </div>
                <div className="ts-service-box-info">
                  <h3 className="service-box-title">
                    <a href="#">Exterior Design</a>
                  </h3>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipiscing elit
                    Integer adipiscing erat
                  </p>
                </div>
              </div>

              <div className="ts-service-box d-flex">
                <div className="ts-service-box-img">
                  <img
                    loading="lazy"
                    src="https://images.placeholders.dev"
                    alt="service-icon"
                  />
                </div>
                <div className="ts-service-box-info">
                  <h3 className="service-box-title">
                    <a href="#">Renovation</a>
                  </h3>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipiscing elit
                    Integer adipiscing erat
                  </p>
                </div>
              </div>

              <div className="ts-service-box d-flex">
                <div className="ts-service-box-img">
                  <img
                    loading="lazy"
                    src="https://images.placeholders.dev"
                    alt="service-icon"
                  />
                </div>
                <div className="ts-service-box-info">
                  <h3 className="service-box-title">
                    <a href="#">Safety Management</a>
                  </h3>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipiscing elit
                    Integer adipiscing erat
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="project-area" className="project-area solid-bg">
        <div className="container">
          <SectionTitle title="Work Of Excellence" subTitle="Recent Projects" />

          <div className="row">
            <div className="col-12">
              <div className="shuffle-btn-group">
                <label className="active" htmlFor="all">
                  <input
                    type="radio"
                    name="shuffle-filter"
                    id="all"
                    value="all"
                    checked={true}
                  />
                  Show All
                </label>
                <label htmlFor="commercial">
                  <input
                    type="radio"
                    name="shuffle-filter"
                    id="commercial"
                    value="commercial"
                  />
                  Commercial
                </label>
                <label htmlFor="education">
                  <input
                    type="radio"
                    name="shuffle-filter"
                    id="education"
                    value="education"
                  />
                  Education
                </label>
                <label htmlFor="government">
                  <input
                    type="radio"
                    name="shuffle-filter"
                    id="government"
                    value="government"
                  />
                  Government
                </label>
                <label htmlFor="infrastructure">
                  <input
                    type="radio"
                    name="shuffle-filter"
                    id="infrastructure"
                    value="infrastructure"
                  />
                  Infrastructure
                </label>
                <label htmlFor="residential">
                  <input
                    type="radio"
                    name="shuffle-filter"
                    id="residential"
                    value="residential"
                  />
                  Residential
                </label>
                <label htmlFor="healthcare">
                  <input
                    type="radio"
                    name="shuffle-filter"
                    id="healthcare"
                    value="healthcare"
                  />
                  Healthcare
                </label>
              </div>

              <div className="row shuffle-wrapper">
                <div className="col-1 shuffle-sizer"></div>

                <div
                  className="col-lg-4 col-md-6 shuffle-item"
                  data-groups='["government","healthcare"]'
                >
                  <div className="project-img-container">
                    <a
                      className="gallery-popup"
                      href="images/projects/project1.jpg"
                      aria-label="project-img"
                    >
                      <img
                        className="img-fluid"
                        src="https://images.placeholders.dev"
                        alt="project-img"
                      />
                      <span className="gallery-icon">
                        <i className="fa fa-plus"></i>
                      </span>
                    </a>
                    <div className="project-item-info">
                      <div className="project-item-info-content">
                        <h3 className="project-item-title">
                          <a href="projects-single.html">
                            Capital Teltway Building
                          </a>
                        </h3>
                        <p className="project-cat">Commercial, Interiors</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className="col-lg-4 col-md-6 shuffle-item"
                  data-groups='["healthcare"]'
                >
                  <div className="project-img-container">
                    <a
                      className="gallery-popup"
                      href="images/projects/project2.jpg"
                      aria-label="project-img"
                    >
                      <img
                        className="img-fluid"
                        src="https://images.placeholders.dev"
                        alt="project-img"
                      />
                      <span className="gallery-icon">
                        <i className="fa fa-plus"></i>
                      </span>
                    </a>
                    <div className="project-item-info">
                      <div className="project-item-info-content">
                        <h3 className="project-item-title">
                          <a href="projects-single.html">Ghum Touch Hospital</a>
                        </h3>
                        <p className="project-cat">Healthcare</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className="col-lg-4 col-md-6 shuffle-item"
                  data-groups='["infrastructure","commercial"]'
                >
                  <div className="project-img-container">
                    <a
                      className="gallery-popup"
                      href="images/projects/project3.jpg"
                      aria-label="project-img"
                    >
                      <img
                        className="img-fluid"
                        src="https://images.placeholders.dev"
                        alt="project-img"
                      />
                      <span className="gallery-icon">
                        <i className="fa fa-plus"></i>
                      </span>
                    </a>
                    <div className="project-item-info">
                      <div className="project-item-info-content">
                        <h3 className="project-item-title">
                          <a href="projects-single.html">TNT East Facility</a>
                        </h3>
                        <p className="project-cat">Government</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className="col-lg-4 col-md-6 shuffle-item"
                  data-groups='["education","infrastructure"]'
                >
                  <div className="project-img-container">
                    <a
                      className="gallery-popup"
                      href="images/projects/project4.jpg"
                      aria-label="project-img"
                    >
                      <img
                        className="img-fluid"
                        src="https://images.placeholders.dev"
                        alt="project-img"
                      />
                      <span className="gallery-icon">
                        <i className="fa fa-plus"></i>
                      </span>
                    </a>
                    <div className="project-item-info">
                      <div className="project-item-info-content">
                        <h3 className="project-item-title">
                          <a href="projects-single.html">
                            Narriot Headquarters
                          </a>
                        </h3>
                        <p className="project-cat">Infrastructure</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className="col-lg-4 col-md-6 shuffle-item"
                  data-groups='["infrastructure","education"]'
                >
                  <div className="project-img-container">
                    <a
                      className="gallery-popup"
                      href="images/projects/project5.jpg"
                      aria-label="project-img"
                    >
                      <img
                        className="img-fluid"
                        src="https://images.placeholders.dev"
                        alt="project-img"
                      />
                      <span className="gallery-icon">
                        <i className="fa fa-plus"></i>
                      </span>
                    </a>
                    <div className="project-item-info">
                      <div className="project-item-info-content">
                        <h3 className="project-item-title">
                          <a href="projects-single.html">Kalas Metrorail</a>
                        </h3>
                        <p className="project-cat">Infrastructure</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className="col-lg-4 col-md-6 shuffle-item"
                  data-groups='["residential"]'
                >
                  <div className="project-img-container">
                    <a
                      className="gallery-popup"
                      href="images/projects/project6.jpg"
                      aria-label="project-img"
                    >
                      <img
                        className="img-fluid"
                        src="https://images.placeholders.dev"
                        alt="project-img"
                      />
                      <span className="gallery-icon">
                        <i className="fa fa-plus"></i>
                      </span>
                    </a>
                    <div className="project-item-info">
                      <div className="project-item-info-content">
                        <h3 className="project-item-title">
                          <a href="projects-single.html">
                            Ancraft Avenue House
                          </a>
                        </h3>
                        <p className="project-cat">Residential</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-12">
              <div className="general-btn text-center">
                <a className="btn btn-primary" href="projects.html">
                  View All Projects
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="content">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <h3 className="column-title">Testimonials</h3>

              <div id="testimonial-slide" className="testimonial-slide">
                <div className="item">
                  <div className="quote-item">
                    <span className="quote-text">
                      Question ran over her cheek When she reached the first
                      hills of the Italic Mountains, she had a last view back on
                      the skyline of her hometown Bookmarksgrove, the headline
                      of Alphabet Village and the subline of her own road.
                    </span>

                    <div className="quote-item-footer">
                      <img
                        loading="lazy"
                        className="testimonial-thumb"
                        src="images/clients/testimonial1.png"
                        alt="testimonial"
                      />
                      <div className="quote-item-info">
                        <h3 className="quote-author">Gabriel Denis</h3>
                        <span className="quote-subtext">Chairman, OKT</span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="item">
                  <div className="quote-item">
                    <span className="quote-text">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                      sed do eiusmod tempor inci done idunt ut labore et dolore
                      magna aliqua. Ut enim ad minim veniam, quis nostrud
                      exercitoa tion ullamco laboris nisi aliquip consequat.
                    </span>

                    <div className="quote-item-footer">
                      <img
                        loading="lazy"
                        className="testimonial-thumb"
                        src="images/clients/testimonial2.png"
                        alt="testimonial"
                      />
                      <div className="quote-item-info">
                        <h3 className="quote-author">Weldon Cash</h3>
                        <span className="quote-subtext">CFO, First Choice</span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="item">
                  <div className="quote-item">
                    <span className="quote-text">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                      sed do eiusmod tempor inci done idunt ut labore et dolore
                      magna aliqua. Ut enim ad minim veniam, quis nostrud
                      exercitoa tion ullamco laboris nisi ut commodo consequat.
                    </span>

                    <div className="quote-item-footer">
                      <img
                        loading="lazy"
                        className="testimonial-thumb"
                        src="images/clients/testimonial3.png"
                        alt="testimonial"
                      />
                      <div className="quote-item-info">
                        <h3 className="quote-author">Minter Puchan</h3>
                        <span className="quote-subtext">Director, AKT</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-lg-6 mt-5 mt-lg-0">
              <h3 className="column-title">Happy Clients</h3>

              <div className="row all-clients">
                <div className="col-sm-4 col-6">
                  <figure className="clients-logo">
                    <a href="#!">
                      <img
                        loading="lazy"
                        className="img-fluid"
                        src="images/clients/client1.png"
                        alt="clients-logo"
                      />
                    </a>
                  </figure>
                </div>

                <div className="col-sm-4 col-6">
                  <figure className="clients-logo">
                    <a href="#!">
                      <img
                        loading="lazy"
                        className="img-fluid"
                        src="images/clients/client2.png"
                        alt="clients-logo"
                      />
                    </a>
                  </figure>
                </div>

                <div className="col-sm-4 col-6">
                  <figure className="clients-logo">
                    <a href="#!">
                      <img
                        loading="lazy"
                        className="img-fluid"
                        src="images/clients/client3.png"
                        alt="clients-logo"
                      />
                    </a>
                  </figure>
                </div>

                <div className="col-sm-4 col-6">
                  <figure className="clients-logo">
                    <a href="#!">
                      <img
                        loading="lazy"
                        className="img-fluid"
                        src="images/clients/client4.png"
                        alt="clients-logo"
                      />
                    </a>
                  </figure>
                </div>
                <div className="col-sm-4 col-6">
                  <figure className="clients-logo">
                    <a href="#!">
                      <img
                        loading="lazy"
                        className="img-fluid"
                        src="images/clients/client5.png"
                        alt="clients-logo"
                      />
                    </a>
                  </figure>
                </div>

                <div className="col-sm-4 col-6">
                  <figure className="clients-logo">
                    <a href="#!">
                      <img
                        loading="lazy"
                        className="img-fluid"
                        src="images/clients/client6.png"
                        alt="clients-logo"
                      />
                    </a>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <Footer />
    </div>
  );
}
